package exam;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith (value = Parameterized.class)
public class TestExam {

	int n, expected;
	JunitAndMock jM;
	
//	@Parameters
//	public static parameters (Collection <Object[] []>numbers() {
//		return   <Object [] [] >	
//		{
//	}); 
	
	public TestExam(int n, int expected) {
		this.n = n;
		this.expected = expected;
	}
	
	@Before
	public void setUp() {
		jM = new JunitAndMock();
	}
	
	@After
	public void tearDown() {
		jM = null;
	}
	
	@Test
	public void testExam() {
		assertEquals(expected, jM.exam(n));
	}

}

